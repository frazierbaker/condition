# condition

_condition your data_

Condition is a python3 package for the normalization, resampling, and splitting of TSV datasets for the purpose of training and evaluating machine learning models.  Condition is built to work with tensorflow and keras.  Condition has 3 core functions that it fulfills:
* Data Generation & Resampling `condition.Generator`
* Normalization `condition.Layer`
* Training & Validation `condition.Runner`
